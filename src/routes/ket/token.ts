import { Router, Request, Response } from 'express';
import * as HttpStatus from 'http-status-codes';
import * as moment from 'moment';
import { TokensModel } from '../../models/ket/token'
const hcode = process.env.HCODE;
const router: Router = Router();
const tokensModel = new TokensModel();

router.get('/', async (req: Request, res: Response, next) => {
  res.send({ ok: true, message: 'Welcome to SmartRefer Clinent!', code: HttpStatus.OK });
});

router.get('/select/:hcode', async (req, res, next) => {
  let hcode = req.params.hcode;
  try {
    let res_token: any = await tokensModel.getTokens(hcode);
    res.send(res_token);
  } catch (error) {
    res.send({ ok: false, error: error });
  }
});

router.post('/insert', async (req, res, next) => {
  let info_ = req.body;
  let info = {
    "hcode": info_.hcode,
    "line_name": info_.line_name,
    "line_token": info_.line_token
  }
  try {
    let res_token: any = await tokensModel.insert(info);
    res.send(res_token);
  } catch (error) {
    res.send({ ok: false, error: error });
  }
});

router.put('/update/:hcode', async (req, res, next) => {
  let hcode = req.params.hcode;
  let info_ = req.body;
  let info = {
    "line_name": info_.line_name,
    "line_token": info_.line_token
  }
  try {
    let res_token: any = await tokensModel.update(hcode, info);
    res.send(res_token);
  } catch (error) {
    res.send({ ok: false, error: error });
  }
});

export default router;