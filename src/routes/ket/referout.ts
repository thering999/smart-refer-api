import { Router, Request, Response } from 'express';
import * as HttpStatus from 'http-status-codes';
import * as moment from 'moment';
import { RerferOutModel } from '../../models/ket/referout'
import { BotlineModel } from '../../models/botline'
import { TokensModel } from '../../models/ket/token'

const hcode = process.env.HCODE;
const router: Router = Router();
const rerferOutModel = new RerferOutModel();
const botlineModel = new BotlineModel();
const tokensModel = new TokensModel();


router.get('/', async (req: Request, res: Response, next) => {
    res.send({ ok: true, message: 'Welcome to SmartRefer Clinent!', code: HttpStatus.OK });
});

router.get('/selectOne/:refer_no', async (req, res, next) => {
    let refer_no = req.params.refer_no;
    try {
        let res_: any = await rerferOutModel.rfout_selectOne(refer_no);
        res.send(res_);
    } catch (error) {
        res.send({ ok: false, error: error });
    }
});

router.get('/select/:hcode/:sdate/:edate/:limit', async (req, res, next) => {
    let limit = req.params.limit;
    let hcode = req.params.hcode;
    let sdate = req.params.sdate;
    let edate = req.params.edate;

    try {
        let res_: any = await rerferOutModel.rfout_select(hcode, sdate, edate, limit);
        res.send(res_);
    } catch (error) {
        res.send({ ok: false, error: error });
    }
});

router.get('/selectLimit/:limit', async (req, res, next) => {
    let limit = req.params.limit;
    try {
        let res_: any = await rerferOutModel.rfout_selectLimit(limit);
        res.send(res_);
    } catch (error) {
        res.send({ ok: false, error: error });
    }
});

router.post('/insert', async (req, res, next) => {
    let info_ = req.body.rows;
    if (info_) {
        let res_: any = await rerferOutModel.rfInsert(info_);
        res.send({ ok: true, res_ });
    } else {
        res.send({ ok: false });
    }

});

router.put('/update/:refer_no', async (req, res, next) => {
    let dbRf = req.dbRf;
    let refer_no = req.params.refer_no;
    let _info_ = req.body.rows;
    try {
        let res_: any = await rerferOutModel.rfUpdate(refer_no, _info_);
        res.send({ ok: true, res_ });
    } catch (error) {
        res.send({ ok: false, error: error });
    }
});

router.get('/receive/:refer_no', async (req, res, next) => {
    let refer_no = req.params.refer_no;
    try {
        let res_: any = await rerferOutModel.rfReceive(refer_no);
        res.send(res_);
    } catch (error) {
        res.send({ ok: false, error: error });
    }
});

router.get('/count_referout/:hcode', async (req, res, next) => {
    let hcode = req.params.hcode;
    try {
        let res_: any = await rerferOutModel.count_referout(hcode);
        res.send(res_);
    } catch (error) {
        res.send({ ok: false, error: error });
    }
});

router.get('/count_referout_reply/:hcode', async (req, res, next) => {
    let hcode = req.params.hcode;
    try {
        let res_: any = await rerferOutModel.count_referout_reply(hcode);
        res.send(res_);
    } catch (error) {
        res.send({ ok: false, error: error });
    }
});

router.get('/select_reply/:hcode/:sdate/:edate/:limit', async (req, res, next) => {
    let limit = req.params.limit;
    let hcode = req.params.hcode;
    let sdate = req.params.sdate;
    let edate = req.params.edate;

    try {
        let res_: any = await rerferOutModel.rfout_select_reply(hcode, sdate, edate, limit);
        res.send(res_);
    } catch (error) {
        res.send({ ok: false, error: error });
    }
});

router.get('/count_report/:stdate/:endate/:hcode', async (req, res, next) => {
    let hcode = req.params.hcode;
    let stdate = req.params.stdate;
    let endate = req.params.endate;
    try {
        let res_: any = await rerferOutModel.count_report(stdate, endate, hcode);
        res.send(res_);
    } catch (error) {
        res.send({ ok: false, error: error });
    }
});

router.get('/count_report_reply/:stdate/:endate/:hcode', async (req, res, next) => {
    let hcode = req.params.hcode;
    let stdate = req.params.stdate;
    let endate = req.params.endate;
    try {
        let res_: any = await rerferOutModel.count_report_reply(stdate, endate, hcode);
        res.send(res_);
    } catch (error) {
        res.send({ ok: false, error: error });
    }
});

router.get('/delete/:refer_no', async (req, res, next) => {
    let refer_no = req.params.refer_no;
    try {
        let res_: any = await rerferOutModel.del_referout(refer_no);
        res.send({ ok: true, res_ });
    } catch (error) {
        res.send({ ok: false, error: error });
    }
});

export default router;