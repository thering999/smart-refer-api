import { Router, Request, Response } from 'express';
import * as HttpStatus from 'http-status-codes';
import * as moment from 'moment';
import { TypeptModel } from '../../models/ket/typept'
const hcode = process.env.HCODE;
const router: Router = Router();
const typeptModel = new TypeptModel();

router.get('/', async (req: Request, res: Response, next) => {
  res.send({ ok: true, message: 'Welcome to SmartRefer Clinent!', code: HttpStatus.OK });
});


router.get('/select', async (req, res, next) => {
  let refer_no = req.params.refer_no;
  try {
    let res_: any = await typeptModel.typept();
    res.send(res_);
  } catch (error) {
    res.send({ ok: false, error: error });
  }
});

export default router;