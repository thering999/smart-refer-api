import { Router, Request, Response } from 'express';
import * as HttpStatus from 'http-status-codes';
import * as moment from 'moment';
import { AttachmentTypeModel } from '../../models/ket/attachment_type'
const hcode = process.env.HCODE;
const router: Router = Router();
const attachmentTypeModel = new AttachmentTypeModel();

router.get('/', async (req: Request, res: Response, next) => {
  res.send({ ok: true, message: 'Welcome to SmartRefer Clinent!', code: HttpStatus.OK });
});

router.get('/select', async (req, res, next) => {
  let dbRf = req.dbRf;
  try {
    let res_: any = await attachmentTypeModel.select();
    res.send(res_);
  } catch (error) {
    res.send({ ok: false, error: error });
  }
});

export default router;