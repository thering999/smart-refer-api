import { Router, Request, Response } from 'express';
import * as HttpStatus from 'http-status-codes';
import * as moment from 'moment';
import { StrengthModel } from '../../models/ket/strength'
const hcode = process.env.HCODE;
const router: Router = Router();
const strengthModel = new StrengthModel();

router.get('/', async (req: Request, res: Response, next) => {
  res.send({ ok: true, message: 'Welcome to SmartRefer Clinent!', code: HttpStatus.OK });
});


router.get('/select', async (req, res, next) => {
  try {
    let res_: any = await strengthModel.strength();
    res.send(res_);
  } catch (error) {
    res.send({ ok: false, error: error });
  }
});

export default router;