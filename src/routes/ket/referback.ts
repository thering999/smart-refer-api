import { Router, Request, Response } from 'express';
import * as HttpStatus from 'http-status-codes';
import * as moment from 'moment';
import { RerferBackModel } from '../../models/ket/referback'
const hcode = process.env.HCODE;
const router: Router = Router();
const rerferBackModel = new RerferBackModel();

router.get('/', async (req: Request, res: Response, next) => {
    res.send({ ok: true, message: 'Welcome to SmartRefer Clinent!', code: HttpStatus.OK });
});

router.get('/selectOne/:refer_no', async (req, res, next) => {
    let refer_no = req.params.refer_no;
    try {
        let res_: any = await rerferBackModel.rfback_selectOne(refer_no);
        res.send(res_);
    } catch (error) {
        res.send({ ok: false, error: error });
    }
});

router.get('/select/:hcode/:sdate/:edate/:limit', async (req, res, next) => {
    let limit = req.params.limit;
    let hcode = req.params.hcode;
    let sdate = req.params.sdate;
    let edate = req.params.edate;

    try {
        let res_: any = await rerferBackModel.rfback_select(hcode, sdate, edate, limit);
        res.send(res_);
    } catch (error) {
        res.send({ ok: false, error: error });
    }
});


router.post('/insert', async (req, res, next) => {
    let dbRf = req.dbRf;
    let info_ = req.body.rows;
    try {
        let res_: any = await rerferBackModel.rfbackInsert(info_);
        res.send({ ok: true, res_ });
    } catch (error) {
        res.send({ ok: false, error: error });
    }
});

router.put('/update/:refer_no', async (req, res, next) => {
    let refer_no = req.params.refer_no;
    let _info_ = req.body.rows;
    try {
        let res_: any = await rerferBackModel.rfbackUpdate(refer_no, _info_);
        res.send({ ok: true, res_ });
    } catch (error) {
        res.send({ ok: false, error: error });
    }
});

router.get('/receive/:refer_no', async (req, res, next) => {
    let dbRf = req.dbRf;
    let refer_no = req.params.refer_no;
    try {
        let res_: any = await rerferBackModel.rfbackReceive(refer_no);
        res.send(res_);
    } catch (error) {
        res.send({ ok: false, error: error });
    }
});

router.get('/count_referback/:hcode', async (req, res, next) => {
    let hcode = req.params.hcode;
    try {
        let res_: any = await rerferBackModel.count_referback(hcode);
        res.send(res_);
    } catch (error) {
        res.send({ ok: false, error: error });
    }
});

router.get('/count_referback_reply/:hcode', async (req, res, next) => {
    let hcode = req.params.hcode;
    try {
        let res_: any = await rerferBackModel.count_referback_reply(hcode);
        res.send(res_);
    } catch (error) {
        res.send({ ok: false, error: error });
    }
});

router.get('/select_reply/:hcode/:sdate/:edate/:limit', async (req, res, next) => {
    let dbRf = req.dbRf;
    let limit = req.params.limit;
    let hcode = req.params.hcode;
    let sdate = req.params.sdate;
    let edate = req.params.edate;

    try {
        let res_: any = await rerferBackModel.rfback_select_reply(hcode, sdate, edate, limit);
        res.send(res_);
    } catch (error) {
        res.send({ ok: false, error: error });
    }
});

router.get('/count_report/:stdate/:endate/:hcode', async (req, res, next) => {
    let dbRf = req.dbRf;
    let hcode = req.params.hcode;
    let stdate = req.params.stdate;
    let endate = req.params.endate;
    try {
        let res_: any = await rerferBackModel.count_report(stdate, endate, hcode);
        res.send(res_);
    } catch (error) {
        res.send({ ok: false, error: error });
    }
});

router.get('/count_report_reply/:stdate/:endate/:hcode', async (req, res, next) => {
    let dbRf = req.dbRf;
    let hcode = req.params.hcode;
    let stdate = req.params.stdate;
    let endate = req.params.endate;
    try {
        let res_: any = await rerferBackModel.count_report_reply(stdate, endate, hcode);
        res.send(res_);
    } catch (error) {
        res.send({ ok: false, error: error });
    }
});

router.get('/delete/:refer_no', async (req, res, next) => {
    let refer_no = req.params.refer_no;
    try {
        let res_: any = await rerferBackModel.del_referback(refer_no);
        res.send({ ok: true, res_ });
    } catch (error) {
        res.send({ ok: false, error: error });
    }
});

export default router;