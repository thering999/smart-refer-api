import { Router, Request, Response } from 'express';
import * as HttpStatus from 'http-status-codes';
import * as moment from 'moment';
import { CauseReferbackModel } from '../../models/ket/cause_referback'
const hcode = process.env.HCODE;
const router: Router = Router();
const causeReferbackModel = new CauseReferbackModel();

router.get('/', async (req: Request, res: Response, next) => {
  res.send({ ok: true, message: 'Welcome to SmartRefer Clinent!', code: HttpStatus.OK });
});


router.get('/select', async (req, res, next) => {
  try {
    let res_: any = await causeReferbackModel.causeReferback();
    res.send(res_);
  } catch (error) {
    res.send({ ok: false, error: error });
  }
});

export default router;