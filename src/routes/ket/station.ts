import { Router, Request, Response } from 'express';
import * as HttpStatus from 'http-status-codes';
import * as moment from 'moment';
import { StationModel } from '../../models/ket/station'
const hcode = process.env.HCODE;
const router: Router = Router();
const stationModel = new StationModel();

router.get('/', async (req: Request, res: Response, next) => {
  res.send({ ok: true, message: 'Welcome to SmartRefer Clinent!', code: HttpStatus.OK });
});


router.get('/select', async (req, res, next) => {
  try {
    let res_: any = await stationModel.station();
    res.send(res_);
  } catch (error) {
    res.send({ ok: false, error: error });
  }
});

export default router;