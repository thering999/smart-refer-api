import { Router, Request, Response } from 'express';
import * as HttpStatus from 'http-status-codes';
import * as moment from 'moment';
import { VerSionModel } from '../../models/ket/version'
import { BotlineModel } from '../../models/botline'

const hcode = process.env.HIS_CODE;
const router: Router = Router();
const verSionModel = new VerSionModel();
const botlineModel = new BotlineModel();

router.get('/', async (req: Request, res: Response, next) => {
  res.send({ ok: true, message: 'Welcome to SmartRefer Clinent!', code: HttpStatus.OK });
});


router.get('/select', async (req, res, next) => {
  let refer_no = req.params.refer_no;
  try {
    let res_: any = await verSionModel.typept();
    res.send(res_);
  } catch (error) {
    let _message = `สถานบริการ ${hcode} เชื่อมต่อระบบ smartrefer servet ket10 ไม่ได้`;
    let lineBot = await botlineModel.botLine(_message, 'EmP6lteGQibDC9Iqy0RcJMlYn4mnKQXyewpa2hlVC5t');
    let info = {
      "id": 0,
      "version_web": "v0.0.0",
      "detail": "เชื่อมต่อระบบ smartrefer servet ket10 ไม่ได้",
      "d_update": "2020-01-01T17:00:00.000Z"
    }
    res.send([info]);
  }
});

export default router;