import { Router, Request, Response } from 'express';
import * as HttpStatus from 'http-status-codes';
import * as moment from 'moment';
import { EvaluateModel } from '../../models/ket/evaluate'
const hcode = process.env.HCODE;
const router: Router = Router();
const evaluateModel = new EvaluateModel();

router.get('/', async (req: Request, res: Response, next) => {
  res.send({ ok: true, message: 'Welcome to SmartRefer Clinent!', code: HttpStatus.OK });
});

router.get('/select', async (req, res, next) => {
  try {
    let res_: any = await evaluateModel.select();
    res.send(res_);
  } catch (error) {
    res.send({ ok: false, error: error });
  }
});

router.get('/select_eva_detail/:refer_no', async (req, res, next) => {
  let refer_no = req.params.refer_no;
  try {
    let res_: any = await evaluateModel.select_eva_detail(refer_no);
    res.send(res_);
  } catch (error) {
    res.send({ ok: false, error: error });
  }
});

router.post('/insert_eva_detail', async (req, res, next) => {
  try {
    let res_: any = await evaluateModel.eva_detail(req.body.rows);
    res.send(res_);
  } catch (error) {
    res.send({ ok: false, error: error });
  }

});

export default router;