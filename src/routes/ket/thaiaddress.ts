import { Router, Request, Response } from 'express';
import * as HttpStatus from 'http-status-codes';
import * as moment from 'moment';
import { ThaiAddresshModel } from '../../models/ket/thaiaddress'
const hcode = process.env.HCODE;
const router: Router = Router();
const thaiAddresshModel = new ThaiAddresshModel();

router.get('/', async (req: Request, res: Response, next) => {
  res.send({ ok: true, message: 'Welcome to SmartRefer Clinent!', code: HttpStatus.OK });
});


router.get('/select/:chwpart/:amppart/:tmbpart', async (req, res, next) => {
  let chwpart = req.params.chwpart;
  let amppart = req.params.amppart;
  let tmbpart = req.params.tmbpart;
  try {
    let res_: any = await thaiAddresshModel.thaiAddress(chwpart, amppart, tmbpart);
    res.send(res_);
  } catch (error) {
    res.send({ ok: false, error: error });
  }
});

router.get('/select_full/:chwpart/:amppart/:tmbpart/:moopart', async (req, res, next) => {
  let dbRf = req.dbRf;
  let chwpart = req.params.chwpart;
  let amppart = req.params.amppart;
  let tmbpart = req.params.tmbpart;
  let moopart = req.params.moopart;
  try {
    let res_: any = await thaiAddresshModel.thaiAddressFull(chwpart, amppart, tmbpart, moopart);
    res.send(res_);
  } catch (error) {
    res.send({ ok: false, error: error });
  }
});

export default router;