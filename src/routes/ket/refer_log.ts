import { Router, Request, Response } from 'express';
import * as HttpStatus from 'http-status-codes';
import * as moment from 'moment';
import { ReferLogModel } from '../../models/ket/refer_log'
const hcode = process.env.HCODE;
const router: Router = Router();
const referLogModel = new ReferLogModel();

router.get('/', async (req: Request, res: Response, next) => {
  res.send({ ok: true, message: 'Welcome to SmartRefer Clinent!', code: HttpStatus.OK });
});


router.get('/select/:refer_no', async (req, res, next) => {
  let refer_no = req.params.refer_no;
  try {
    let res_: any = await referLogModel.referLog(refer_no);
    res.send(res_);
  } catch (error) {
    res.send({ ok: false, error: error });
  }
});

router.get('/inser', async (req, res, next) => {
  let refer_no = req.params.refer_no;
  try {
    let res_: any = await referLogModel.referLog(refer_no);
    res.send(res_);
  } catch (error) {
    res.send({ ok: false, error: error });
  }
});

export default router;