import { Router, Request, Response } from 'express';
import * as HttpStatus from 'http-status-codes';
import * as moment from 'moment';
import { CoOfficeModel } from '../../models/ket/co_office'
const hcode = process.env.HCODE;
const router: Router = Router();
const coOfficeModel = new CoOfficeModel();

router.get('/', async (req: Request, res: Response, next) => {
  res.send({ ok: true, message: 'Welcome to SmartRefer Clinent!', code: HttpStatus.OK });
});


router.get('/selectAll', async (req, res, next) => {
  try {
    let res_: any = await coOfficeModel.select(hcode);
    res.send(res_);
  } catch (error) {
    res.send({ ok: false, error: error });
  }
});

router.get('/select/:hcode', async (req, res, next) => {
  let hcode = req.params.hcode;
  try {
    let res_: any = await coOfficeModel.cooffice(hcode);
    res.send(res_);
  } catch (error) {
    res.send({ ok: false, error: error });
  }
});

router.get('/selectLike/:searchText', async (req, res, next) => {
  let searchText = req.params.searchText;
  try {
    let res_: any = await coOfficeModel.searchText(searchText);
    res.send(res_);
  } catch (error) {
    res.send({ ok: false, error: error });
  }
});

export default router;