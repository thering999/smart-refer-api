import { Router, Request, Response } from 'express';
import * as HttpStatus from 'http-status-codes';
import * as moment from 'moment';
import { AttachmentModel } from '../../models/ket/attachment'
const hcode = process.env.HCODE;
const router: Router = Router();
const attachmentModel = new AttachmentModel();

router.get('/', async (req: Request, res: Response, next) => {
  res.send({ ok: true, message: 'Welcome to SmartRefer Clinent!', code: HttpStatus.OK });
});

router.get('/select/:refer_no', async (req, res, next) => {
  // let dbRf = req.dbRf;
  let refer_no = req.params.refer_no;

  try {
    let res_: any = await attachmentModel.select(refer_no);
    res.send(res_);
  } catch (error) {
    res.send({ ok: false, error: error });
  }
});

export default router;