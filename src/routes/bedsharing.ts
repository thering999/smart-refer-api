import { Router, Request, Response } from 'express';
import * as HttpStatus from 'http-status-codes';
import * as moment from 'moment';
var cron = require('node-cron');
const request = require("request");

// model
import { HisHiModel } from './../models/his_hi.model';
import { HisHomcModel } from './../models/his_homc.model';
import { HisHosxpv3Model } from './../models/his_hosxpv3.model';
import { HisHosxpv4Model } from './../models/his_hosxpv4.model';
import { HisMbaseModel } from './../models/his_mbase.model';

import { BedsharingModel } from './../models/ket/bedsharing';


// const servicesModel = new ServicesModel();
const provider = process.env.HIS_PROVIDER;
const hpspcode = process.env.HIS_CODE;
const url = process.env.HIS_API_URL;
const router: Router = Router();
const bedsharingModel = new BedsharingModel();

// ห้ามแก้ไข //
let hisModel: any;
switch (provider) {
  case 'hosxpv3':
    hisModel = new HisHosxpv3Model();
    break;
  case 'hosxpv4':
    hisModel = new HisHosxpv4Model();
    break;
  case 'hi':
    hisModel = new HisHiModel();
    break;
  case 'homc':
    hisModel = new HisHomcModel();
    break;
  case 'mbase':
    hisModel = new HisMbaseModel();
    break;
  default:
  // hisModel = new HisModel();
}

/* GET home page. */
router.get('/', async (req, res, next) => {
  let dateToday = Date();
  let db = req.db;
  let dbRf = req.dbRf;
  let objBedsharing: any = {};
  let bedsharing = [];
  try {
    let res_bedsharing = await hisModel.getBedsharing(db);
    // console.log(res_bedsharing);
    if (res_bedsharing) {
      res_bedsharing.forEach(async v => {
        let info = {
          "regdate": moment(dateToday).format('YYYY-MM-DD'),
          "time": moment(dateToday).format('HH:mm:ss'),
          "hcode": hpspcode,
          "ward_code": v.ward_code,
          "ward_name": v.ward_name,
          "ward_bed": v.ward_bed,
          "ward_standard": v.ward_standard,
          "ward_pt": v.ward_pt,
          "bor": '0',
          "province": v.province
        }
        let info_ = {
          "regdate": moment(dateToday).format('YYYY-MM-DD'),
          "time": moment(dateToday).format('HH:mm:ss'),
          "ward_name": v.ward_name,
          "ward_bed": v.ward_bed,
          "ward_standard": v.ward_standard,
          "ward_pt": v.ward_pt,
          "bor": '0',
          "province": v.province
        }
        bedsharing.push(info);
        let res_bedsharing_web = await bedsharingModel.getBedsharing(dbRf, hpspcode, info.ward_code)
        console.log('res_bedsharing_web :', res_bedsharing_web);

        if (res_bedsharing_web[0]) {
          console.log('update');
          console.log(info_);
          let res_inser_web = await bedsharingModel.update(dbRf, hpspcode, info.ward_code, info_)
        } else {
          console.log('insert');
          console.log(info);
          let res_updete_web = await bedsharingModel.insert(dbRf, info)
        }
      });
      objBedsharing.bedsharing = bedsharing;
    }
    res.send(objBedsharing);
  } catch (error) {
    res.send({ ok: false, error: error });
  }
});

cron.schedule('*/60 * * * *', async (req, res, next) => {
  console.log('running a task every minute');
  request({
    method: 'GET',
    uri: url + '/bedsharing',
    header: {
      'Content-Type': 'application/x-www-form-urlencoded',
    },
  }, (err, httpResponse, body) => {
    if (err) {
      console.log(err)
    } else {
      console.log(body)
    }
  })

});

export default router;