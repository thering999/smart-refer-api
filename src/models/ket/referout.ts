import * as Knex from 'knex';
const request = require("request");
const urlApi = process.env.SERV_API_URL;

export class RerferOutModel {
    async rfout_select(hcode: any, sdate: any, edate: any, limit: any) {
        return await new Promise((resolve: any, reject: any) => {
            var options = {
                method: 'GET',
                url: `${urlApi}/referout/select/${hcode}/${sdate}/${edate}/${limit}`,
                agentOptions: {
                    rejectUnauthorized: false
                },
                headers:
                {
                    'cache-control': 'no-cache',
                    'content-type': 'application/json',
                },
                json: true
            };

            request(options, function (error, response, body) {
                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });
    }

    async rfout_select_reply(hcode: any, sdate: any, edate: any, limit: any) {
        return await new Promise((resolve: any, reject: any) => {
            var options = {
                method: 'GET',
                url: `${urlApi}/referout/select_reply/${hcode}/${sdate}/${edate}/${limit}`,
                agentOptions: {
                    rejectUnauthorized: false
                },
                headers:
                {
                    'cache-control': 'no-cache',
                    'content-type': 'application/json',
                },
                json: true
            };

            request(options, function (error, response, body) {
                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });

    }
    async rfout_selectOne(refer_no: any) {
        return await new Promise((resolve: any, reject: any) => {
            var options = {
                method: 'GET',
                url: `${urlApi}/referout/selectOne/${refer_no}`,
                agentOptions: {
                    rejectUnauthorized: false
                },
                headers:
                {
                    'cache-control': 'no-cache',
                    'content-type': 'application/json',
                },
                json: true
            };

            request(options, function (error, response, body) {
                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });
    }

    async rfout_selectLimit(limit: any) {
        return await new Promise((resolve: any, reject: any) => {
            var options = {
                method: 'GET',
                url: `${urlApi}/referout/selectLimit/${limit}`,
                agentOptions: {
                    rejectUnauthorized: false
                },
                headers:
                {
                    'cache-control': 'no-cache',
                    'content-type': 'application/json',
                },
                json: true
            };

            request(options, function (error, response, body) {
                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });
    }

    async rfInsert(rows: any) {
        console.log(rows);

        return await new Promise((resolve: any, reject: any) => {
            var options = {
                method: 'POST',
                url: `${urlApi}/referout/insert`,
                agentOptions: {
                    rejectUnauthorized: false
                },
                headers:
                {
                    'cache-control': 'no-cache',
                    'content-type': 'application/json',
                },
                body: { rows: rows }
                ,
                json: true
            };

            request(options, function (error, response, body) {
                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });
    }

    async rfUpdate(refer_no: any, rows: any) {
        return await new Promise((resolve: any, reject: any) => {
            var options = {
                method: 'PUT',
                url: `${urlApi}/referout/update/${refer_no}`,
                agentOptions: {
                    rejectUnauthorized: false
                },
                headers:
                {
                    'cache-control': 'no-cache',
                    'content-type': 'application/json',
                },
                body: { rows: rows }
                ,
                json: true
            };

            request(options, function (error, response, body) {
                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });
    }

    async rfReceive(refer_no: any) {
        return await new Promise((resolve: any, reject: any) => {
            var options = {
                method: 'GET',
                url: `${urlApi}/referout/receive/${refer_no}`,
                agentOptions: {
                    rejectUnauthorized: false
                },
                headers:
                {
                    'cache-control': 'no-cache',
                    'content-type': 'application/json',
                },
                json: true
            };

            request(options, function (error, response, body) {
                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });
    }


    async count_referout(hcode: any) {
        return await new Promise((resolve: any, reject: any) => {
            var options = {
                method: 'GET',
                url: `${urlApi}/referout/count_referout/${hcode}`,
                agentOptions: {
                    rejectUnauthorized: false
                },
                headers:
                {
                    'cache-control': 'no-cache',
                    'content-type': 'application/json',
                },
                json: true
            };

            request(options, function (error, response, body) {
                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });
    }

    async count_referout_reply(hcode: any) {
        return await new Promise((resolve: any, reject: any) => {
            var options = {
                method: 'GET',
                url: `${urlApi}/referout/count_referout_reply/${hcode}`,
                agentOptions: {
                    rejectUnauthorized: false
                },
                headers:
                {
                    'cache-control': 'no-cache',
                    'content-type': 'application/json',
                },
                json: true
            };

            request(options, function (error, response, body) {
                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });
    }

    async count_report(stdate: any, endate: any, hcode: any) {
        return await new Promise((resolve: any, reject: any) => {
            var options = {
                method: 'GET',
                url: `${urlApi}/referout/count_report/${stdate}/${endate}/${hcode}`,
                agentOptions: {
                    rejectUnauthorized: false
                },
                headers:
                {
                    'cache-control': 'no-cache',
                    'content-type': 'application/json',
                },
                json: true
            };

            request(options, function (error, response, body) {
                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });
    }

    async count_report_reply(stdate: any, endate: any, hcode: any) {
        return await new Promise((resolve: any, reject: any) => {
            var options = {
                method: 'GET',
                url: `${urlApi}/referout/count_report_reply/${stdate}/${endate}/${hcode}`,
                agentOptions: {
                    rejectUnauthorized: false
                },
                headers:
                {
                    'cache-control': 'no-cache',
                    'content-type': 'application/json',
                },
                json: true
            };

            request(options, function (error, response, body) {
                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });
    }

    async del_referout(refer_no: any) {
        return await new Promise((resolve: any, reject: any) => {
            var options = {
                method: 'GET',
                url: `${urlApi}/referout/delete/${refer_no}`,
                agentOptions: {
                    rejectUnauthorized: false
                },
                headers:
                {
                    'cache-control': 'no-cache',
                    'content-type': 'application/json',
                },
                json: true
            };

            request(options, function (error, response, body) {
                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });
    }

}