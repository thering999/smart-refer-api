import * as Knex from 'knex';
const request = require("request");
const urlApi = process.env.SERV_API_URL;

export class TokensModel {

  async getTokens(hcode: any) {
    return new Promise((resolve: any, reject: any) => {
      var options = {
        method: 'GET',
        url: `${urlApi}/token/select/${hcode}`,
        agentOptions: {
          rejectUnauthorized: false
        },
        headers:
        {
          'cache-control': 'no-cache',
          'content-type': 'application/json',
        },
        json: true
      };

      request(options, function (error, response, body) {
        if (error) {
          reject(error);
        } else {
          resolve(body);
        }
      });
    });
  }

  async insert(info: any) {
    return new Promise((resolve: any, reject: any) => {
      var options = {
        method: 'POST',
        url: `http://localhost:3016/token/insert`,
        agentOptions: {
          rejectUnauthorized: false
        },
        headers:
        {
          'cache-control': 'no-cache',
          'content-type': 'application/json',
        },
        body: info
        ,
        json: true
      };

      request(options, function (error, response, body) {
        if (error) {
          reject(error);
        } else {
          resolve(body);
        }
      });
    });
  }

  async update(hcode: any, info: any) {
    return new Promise((resolve: any, reject: any) => {
      var options = {
        method: 'PUT',
        url: `http://203.157.166.24/api/refer/token/update/${hcode}`,
        agentOptions: {
          rejectUnauthorized: false
        },
        headers:
        {
          'cache-control': 'no-cache',
          'content-type': 'application/json',
        },
        body: info
        ,
        json: true
      };

      request(options, function (error, response, body) {
        if (error) {
          reject(error);
        } else {
          resolve(body);
        }
      });
    });
  }

}