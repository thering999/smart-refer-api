import * as Knex from 'knex';
const request = require("request");
const urlApi = process.env.SERV_API_URL;

export class TypeptModel {

  async typept() {
    return await new Promise((resolve: any, reject: any) => {
      var options = {
        method: 'GET',
        url: `${urlApi}/typept/select`,
        agentOptions: {
          rejectUnauthorized: false
        },
        headers:
        {
          'cache-control': 'no-cache',
          'content-type': 'application/json',
        },
        json: true
      };

      request(options, function (error, response, body) {
        if (error) {
          reject(error);
        } else {
          resolve(body);
        }
      });
    });
  }

}