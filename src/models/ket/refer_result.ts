import * as Knex from 'knex';
const request = require("request");
const urlApi = process.env.SERV_API_URL;

export class RerferResultModel {

    async list() {
        return await new Promise((resolve: any, reject: any) => {
            var options = {
                method: 'GET',
                url: `${urlApi}/result/select`,
                agentOptions: {
                    rejectUnauthorized: false
                },
                headers:
                {
                    'cache-control': 'no-cache',
                    'content-type': 'application/json',
                },
                json: true
            };

            request(options, function (error, response, body) {
                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });
    }
}