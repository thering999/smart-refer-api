import * as Knex from 'knex';
const request = require("request");
const urlApi = process.env.SERV_API_URL;

export class ThaiAddresshModel {

  async thaiAddress(chwpart: any, amppart: any, tmbpart: any) {
    return await new Promise((resolve: any, reject: any) => {
      var options = {
        method: 'GET',
        url: `${urlApi}/address/select/${chwpart}/${amppart}/${tmbpart}`,
        agentOptions: {
          rejectUnauthorized: false
        },
        headers:
        {
          'cache-control': 'no-cache',
          'content-type': 'application/json',
        },
        json: true
      };

      request(options, function (error, response, body) {
        if (error) {
          reject(error);
        } else {
          resolve(body);
        }
      });
    });
  }

  async thaiAddressFull(chwpart: any, amppart: any, tmbpart: any, moopart: any) {
    return await new Promise((resolve: any, reject: any) => {
      var options = {
        method: 'GET',
        url: `${urlApi}/address/select_full/${chwpart}/${amppart}/${tmbpart}/${moopart}`,
        agentOptions: {
          rejectUnauthorized: false
        },
        headers:
        {
          'cache-control': 'no-cache',
          'content-type': 'application/json',
        },
        json: true
      };

      request(options, function (error, response, body) {
        if (error) {
          reject(error);
        } else {
          resolve(body);
        }
      });
    });
  }

}