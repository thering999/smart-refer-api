import * as Knex from 'knex';
const request = require("request");
const urlApi = process.env.SERV_API_URL;

export class BedsharingModel {
  async getBedsharing(dbRf: Knex, hcode: any, ward_code: any) {
    return await new Promise((resolve: any, reject: any) => {
      var options = {
        method: 'GET',
        url: `${urlApi}/bedsharing/select/${hcode}/${ward_code}`,
        agentOptions: {
          rejectUnauthorized: false
        },
        headers:
        {
          'cache-control': 'no-cache',
          'content-type': 'application/json',
        },
        json: true
      };

      request(options, function (error, response, body) {
        if (error) {
          reject(error);
        } else {
          resolve(body);
        }
      });
    });
  }

  async insert(dbRf: Knex, info: any) {
    return await new Promise((resolve: any, reject: any) => {
      var options = {
        method: 'POST',
        url: `${urlApi}/bedsharing/insert`,
        agentOptions: {
          rejectUnauthorized: false
        },
        headers:
        {
          'cache-control': 'no-cache',
          'content-type': 'application/json',
        },
        body: info
        ,
        json: true
      };

      request(options, function (error, response, body) {
        if (error) {
          reject(error);
        } else {
          resolve(body);
        }
      });
    });
  }

  async update(dbRf: Knex, hcode: any, ward_code: any, info: any) {
    return await new Promise((resolve: any, reject: any) => {
      var options = {
        method: 'PUT',
        url: `${urlApi}/bedsharing/update/${hcode}/${ward_code}`,
        agentOptions: {
          rejectUnauthorized: false
        },
        headers:
        {
          'cache-control': 'no-cache',
          'content-type': 'application/json',
        },
        body: info
        ,
        json: true
      };

      request(options, function (error, response, body) {
        if (error) {
          reject(error);
        } else {
          resolve(body);
        }
      });
    });
  }
}
