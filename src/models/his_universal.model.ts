import Knex = require('knex');
const hospcode = process.env.HIS_CODE;

export class HisUniversalHiModel {

    async getLogin(db: Knex, username: any, password: any) {
        let data = await db.raw(`
        SELECT username , fullname, hcode
        from smartrefer_user
        `);
        return data[0];
    }

    async getServices(db: Knex, hn: any, seq: any) {
        let data = await db.raw(`
        SELECT 
        seq, title_name, first_name, last_name, 
		date_serv, time_serv, department
		from smartrefer_visit
        WHERE hn = '${hn}' and vn = '${seq}'`);
        return data[0];
    }

    async getProfile(db: Knex, hn: any) {
        let data = await db.raw(`
        select hn, cid, title_name,first_name, last_name,
        moopart, addrpart, tmbpart, amppart, chwpart, brthdate
        , age, pttype_id, pttype_name, pttype_no, hospmain
        , hospmain_name, hospsub, hospsub_name, registdate, visitdate
        , father_name, mother_name, couple_name, contact_name,contact_relation, contact_mobile
        FROM smartrefer_person
        WHERE hn ='${hn}'`);
        return data[0];
    }

    async getHospital(db: Knex, hn: any) {
        let data = await db.raw(`
        SELECT provider_code, provider_name 
        FROM smartrefer_hospital`);
        return data[0];
    }

    async getAllergyDetail(db: Knex, hn: any) {
        let data = await db.raw(`
        SELECT namedrug as drug_name, detail as symptom ,entrydate as begin_date,entrydate as daterecord
        FROM smartrefer_allergy 
        WHERE hn ='${hn}'`);
        return data[0];
    }

    async getChronic(db: Knex, hn: any) {
        let data = await db.raw(`
        SELECT icd_code, start_date, icd_name
        FROM smartrefer_chronic 
        WHERE hn ='${hn}'`);
        return data[0];
    }

    async getDiagnosis(db: Knex, hn: any, dateServe: any, seq: any) {
        let data = await db.raw(`
        SELECT seq, date_serv, time_serv
        , icd_code, cd_name
        , diag_type, DiagNote, diagtype_id
        FROM smartrefer_diagnosis  
        WHERE seq ='${seq}'`);
        return data[0];
    }

    async getRefer(db: Knex, hn: any, dateServe: any, seq: any) {
        let data = await db.raw(`
        select eq, an, pid, hn, referno, ReferDate, to_hcode, 
        pttype_id, pttype_name, strength_id,location_name,  station_name, loads_id, loads_name,
        to_hcode_name, refer_cause, ReferTime,
	    d.lcno as doctor,
	    CONCAT(d.fname,' ',d.lname) as doctor_name
        from smartrefer_refer
        where seq ='${seq}'
        `);
        return data[0];
    }

    async getDrugs(db: Knex, hn: any, dateServe: any, seq: any) {
        let data = await db.raw(`
        select seq, date_serv, time_serv, drug_name,
        qty, unit, usage_line1, usage_line2, usage_line3
        FROM smartrefer_drugs
        WHERE seq = '${seq}'`);
        return data[0];
    }

    async getLabs(db: Knex, hn: any, dateServe: any, seq: any) {
        let data = await db.raw(`
        SELECT date_serv, time_serv, labgroup, lab_name, lab_result, unit, standard_result
        FROM smartrefer_lab where seq ='${seq}'`);
        return data[0];
    }

    async getAppointment(db: Knex, hn: any, dateServ: any, seq: any) {
        let data = await db.raw(``);
        return data[0];
    }

    async getVaccine(db: Knex, hn: any) {
        let data = await db.raw(`
        select date_serv, time_serv, vaccine_code, vaccine_name
        from smartrefer_vaccine
        where hn='${hn}'`);
        return data[0];
    }
    async getProcedure(db: Knex, hn: any, dateServe: any, seq: any) {
        let data = await db.raw(`
        SELECT pid, seq, date_serv,	time_serv, procedure_code,	
        procedure_name, start_date, start_time, end_date, end_time
        from smartrefer_procedure
        where seq = '${seq}'
        `);
        return data[0];
    }

    async getNurture(db: Knex, hn: any, dateServe: any, seq: any) {
        let data = await db.raw(`
        select seq, date_serv, time_serv, bloodgrp, weight, height, bmi,
        temperature, pr, rr, sbp, dbp, symptom, depcode, department
        FROM smartrefer_nurture
        WHERE seq = '${seq}'
        `);
        return data[0];
    }


    async getPhysical(db: Knex, hn: any, dateServe: any, seq: any) {
        let data = await db.raw(`
        SELECT seq , pe FROM smartrefer_physical WHERE seq = '${seq}'
        `);
        return data[0];
    }

    async getPillness(db: Knex, hn: any, dateServe: any, seq: any) {
        let data = await db.raw(`
        SELECT seq , hpi FROM smartrefer_pillness WHERE vn = '${seq}'
        `);
        return data[0];
    }

    async getXray(db: Knex, hn: any, dateServe: any, seq: any) {
        let data = await db.raw(`
        SELECT xray_date ,xray_name 
        FROM smartrefer_xray 
        WHERE seq = '${seq}'
        `);
        return data[0];
    }

    async getBedsharing(db: Knex) {
        let data = await db.raw(`
        select ward_code, ward_name, ward_pt, ward_bed, ward_standard 
        from smartrefer_bedsharing 
        `);
        return data[0];
    }

    async getReferOut(db: Knex, start_date: any, end_date: any) {
        let data = await db.raw(`
        select seq, hn, title_name, first_name, last_name, referno, ReferDate, to_hcode, pttype_id,
        pttype_name, strength_id, to_hcode_name, refer_cause, ReferTime, doctor, doctor_name
        from smartrefer_refer
        where date(ReferDate) between '${start_date}' and '${end_date}'  GROUP BY seq`);
        return data[0];
    }

    async getReferBack(db: Knex, start_date: any, end_date: any) {
        let data = await db.raw(`
        select eq, hn, title_name, first_name, last_name, referno, ReferDate,
        to_hcode, o_hcode_name, refer_cause, ReferTime, doctor, doctor_name
        from smartrefer_refer
        where ReferDate between '${start_date}' and '${end_date}'`);
        return data[0];
    }

    async getAppoint(db: Knex, hn: any, app_date: any) {
        let data = await db.raw(`
        SELECT seq, receive_apppoint_date, receive_apppoint_beginttime,
        receive_apppoint_endtime, receive_apppoint_doctor, receive_apppoint_chinic, receive_text,
        receive_by
        from smartrefer_appoint
        WHERE hn ='${hn}' and receive_apppoint_date = '${app_date}'
        `);
        return data[0];
    }
    async getDepartment(db: Knex) {
        let data = await db.raw(`
            SELECT dep_code, dep_name from smartrefer_department 
        `);
        return data[0];

    }
    async getPtHN(db: Knex, cid: any) {
        let data = await db.raw(`

        `);
        return data[0];
    }

}
