import Knex = require('knex');
const hospcode = process.env.HIS_CODE;
var md5 = require('md5');

export class HisHosxpv4PgModel {

  async getLogin(db: Knex, username: any, password: any) {
    let pass = md5(password).toUpperCase();
    let data = await db.raw(`
    select o.loginname as username , 
           name as fullname, 
           (select hospitalcode from opdconfig limit 1) as hcode				
    from opduser o		 
    where  o.loginname = '${username}' 		
           and passweb = '${pass}' 
        `);
    return data.rows;
  }

  async getServices(db: Knex, hn: any, seq: any) {
    let data = await db.raw(`
    SELECT 
          o.vn as seq,
          p.pname as title_name,
          p.fname as first_name,
          p.lname as last_name, 
          o.vstdate as date_serv,
          o.vsttime as time_serv,
          c.department as department
    FROM ovst as o 
    INNER JOIN kskdepartment as c ON c.depcode = o.main_dep
    INNER JOIN patient as p	ON p.hn = o.hn
    WHERE o.hn ='${hn}' and o.vn = '${seq}' 
    UNION
    SELECT
          an.an as seq,
          p.pname as title_name,
          p.fname as first_name,
          p.lname as last_name,
          i.regdate as date_serv,
          i.regtime as time_serv,
          w.ward as department
    FROM an_stat as an
    INNER JOIN ward as w ON w.ward = an.ward
    INNER JOIN patient as p ON p.hn = an.hn
    INNER JOIN ipt as i On i.an = an.an
    WHERE an.hn ='${hn}' and an.an = '${seq}'
    `);
    return data.rows;
  }

  async getProfile(db: Knex, hn: any) {
    let data = await db.raw(`
    select p.hn as hn, 
    p.cid as cid, 
    p.pname as title_name,
    p.fname as first_name,
    p.lname as last_name,
    s.name as sex,
    oc.name as job,
    p.moopart,
    p.addrpart,
    p.tmbpart,
    p.amppart,
    p.chwpart,
    p.birthday,
    REPLACE(age(current_date, p.birthday) ::text,'years','-') as age,
    o.pttype as pttype_id,
    t.name as pttype_name,
    o.pttypeno as pttype_no,
    o.hospmain,
    c.name as hospmain_name,
    o.hospsub,
    h.name as hospsub_name,
    p.firstday as registdate,
    p.last_visit as visitdate,
    p.fathername as father_name,
    p.mathername as mother_name,
    p.informrelation as contact_name,
    p.informtel as contact_mobile,
    oc.name as occupation
    FROM patient as p 
    INNER JOIN ovst as o on o.hn=p.hn
    left join pttype as t on o.pttype=t.pttype
    left join hospcode as c on o.hospmain=c.hospcode
    left join hospcode as h on o.hospsub=h.hospcode
    LEFT JOIN sex s on s.code = p.sex 
    LEFT JOIN occupation oc on oc.occupation = p.occupation
    WHERE p.hn = '${hn}'	 and p.last_visit = o.vstdate
    `);
    return data.rows;
  }

  async getHospital(db: Knex, hn: any) {
    let data = await db.raw(`
      SELECT hospitalcode as provider_code,
             hospitalname as provider_name 
      from opdconfig limit 1
     `);
    return data.rows;
  }

  async getAllergyDetail(db: Knex, hn: any) {
    let data = await db.raw(`
    SELECT agent as drug_name, 
           symptom as symptom ,
           begin_date as begin_date,
           date(entry_datetime) as daterecord
    FROM opd_allergy
    WHERE hn ='${hn}'
    `);
    return data.rows;
  }

  async getChronic(db: Knex, hn: any) {
    let data = await db.raw(`
    SELECT c.icd10 as icd_code,
           cm.regdate as start_date, 
           (CASE WHEN i.tname!='' THEN i.tname ELSE '-' END) as icd_name
    FROM clinicmember as cm 
    inner join clinic c on cm.clinic = c.clinic
    INNER JOIN icd101 as i on i.code = c.icd10
    WHERE cm.hn ='${hn}'
    `);
    return data.rows;
  }

  async getDiagnosis(db: Knex, hn: any, dateServe: any, seq: any) {
    let data = await db.raw(`
        SELECT o.vn as seq, 
        to_char(o.vstdate :: date,'yyyy-mm-dd') as date_serv,
        to_char(o.vsttime :: time,'HH:mm:ss') as time_serv,
        d.icd10 as icd_code,
        i.name as icd_name,
        d.diagtype as diag_type, 
        dt.provisional_dx_text as DiagNote,           
        '' as diagtype_id          
        FROM ovst as o
        INNER JOIN ovstdiag d on d.vn = o.vn
        INNER JOIN icd101 as i on i.code = d.icd10
        left JOIN ovstdiag_text dt on o.vn = dt.vn 
        WHERE d.vn ='${seq}'
        UNION
        SELECT  i.an as seq, 
        to_char(i.dchdate :: date,'yyyy-mm-dd') as date_serv,
        to_char(i.dchtime :: time,'HH:mm:ss') as time_serv,
        id.icd10 as icd_code,
        ic.name as icd_name,
        id.diagtype as diag_type, 
        ic.name as DiagNote,           
        id.diagtype as diagtype_id          
        FROM ipt i
        INNER JOIN iptdiag id  on id.an = i.an
        INNER JOIN icd101 ic  on ic.code = id.icd10
        WHERE i.an = '${seq}'
        `);
    return data.rows;
  }

  async getRefer(db: Knex, hn: any, dateServe: any, seq: any) {
    let data = await db.raw(`
      select 
      (CASE WHEN LENGTH(r.vn) >=  9 THEN r.vn ELSE o2.vn END) as seq,
           o2.an as an,
           r.hn as pid, 
           r.hn as hn,
           replace(r.refer_number :: text,'/','-') as referno,
           r.refer_date as ReferDate, 
           r.refer_hospcode as to_hcode, 
           (CASE WHEN r.referout_emergency_type_id='3' THEN 1 ELSE 2 END) as pttype_id,
           r.refer_cause as strength_id,
          (case r.refer_cause 
                when 1 then 'Life threatening'
                when 2 then 'Emergency'
                when 3 then 'Urgent'
                when 4 then 'Acute'
                when 5 then 'Non acute'         
                else '' end 
          ) as strength_name,
          r.refer_point as location_name,			
          r.refer_point as station_name,
          (select name from hospcode where hospcode = refer_hospcode) as to_hcode_name,
          (select name from refer_cause where id = refer_cause) as refer_cause,
          refer_time as ReferTime,
          (select licenseno  from doctor where code = r.doctor) as doctor,
          (select name  from doctor where code = r.doctor) as doctor_name   
      from referout as r
      left outer join ovst o on r.vn = o.vn
      left outer join ovst o2 on r.vn = o2.an
      where  (CASE WHEN LENGTH(r.vn) >=  9 THEN r.vn ELSE o2.vn END) = '${seq}'	
        `);
    return data.rows;
  }

  async getDrugs(db: Knex, hn: any, dateServe: any, seq: any) {
    let data = await db.raw(`
      SELECT a.seq,a.date_serv,a.time_serv,a.drug_name,a.qty,a.unit,a.usage_line1,a.usage_line2,a.usage_line3
      FROM (
      SELECT 
				   i.vn as seq,
           to_char(i.rxdate :: date,'yyyy-mm-dd') as date_serv,
           to_char(i.rxtime :: time,'HH:mm:ss') as time_serv,
           d.name as drug_name,
           sum(i.qty) as qty,
           d.units as unit ,
           g.name1 as usage_line1,
           g.name2 as usage_line2,
           g.name3  as usage_line3
      FROM ovst o
      inner join opitemrece as i on o.vn = i.vn
      inner Join drugitems as d ON i.icode = d.icode
			inner join drugusage as g on g.drugusage = d.drugusage
      WHERE i.vn = '${seq}'
      GROUP BY i.vn,i.rxtime,d.name,d.units, i.icode,i.rxdate,g.name1,g.name2,g.name3
      union
      select 
					 o.vn as seq,
           to_char(i.rxdate :: date,'yyyy-mm-dd') as date_serv,
           to_char(i.rxtime :: time,'HH:mm:ss') as time_serv,
           d.name as drug_name,
           sum(i.qty) as qty,
           d.units as unit ,
           g.name1 as usage_line1 ,
           g.name2 as usage_line2,
           g.name3 as usage_line3
      FROM ovst o
      inner join opitemrece as i on o.an = i.an
      Inner Join drugitems as d ON i.icode = d.icode
			inner join drugusage as g on g.drugusage = d.drugusage
      WHERE i.an = '${seq}'
      GROUP BY o.vn,i.rxtime,d.name,d.units, i.icode,i.rxdate,g.name1,g.name2,g.name3
      ) a
      WHERE a.seq is not NULL
    `);
    return data.rows;
  }

  async getLabs(db: Knex, hn: any, dateServe: any, seq: any) {
    let data = await db.raw(`
    select receive_date as date_serv,
           receive_time as time_serv,
           form_name as labgroup,
           lab_items_name_ref as lab_name,
           lab_order_result as lab_result,
           li.lab_items_unit as unit,
           lo.lab_items_normal_value_ref as standard_result
    from ovst o
    inner join lab_head lh on o.vn = lh.vn or o.an = lh.vn
    inner join lab_order lo on lh.lab_order_number = lo.lab_order_number
    inner join lab_items li on li.lab_items_code = lo.lab_items_code
    where lh.vn = '${seq}' 
    `);
    return data.rows;
  }

  async getAppointment(db: Knex, hn: any, dateServ: any, seq: any) {
    let data = await db.raw(`
    SELECT o.vn as seq, 
           o.vstdate as date_serv, 
           o.nextdate as date, 
           o.nexttime as time, 
           (select name from clinic where clinic = o.clinic ) as department, 
           o.app_cause as detail, 
           to_char(ovst.vsttime :: time,'HH:mm:ss') as time_serv
    FROM oapp as o 
    INNER JOIN ovst on ovst.vn = o.vn
    WHERE o.vn ='${seq}'
    `);
    return data.rows;
  }

  async getVaccine(db: Knex, hn: any) {
    let data = await db.raw(`
    SELECT o.vstdate as date_serv,
           o.vsttime as time_serv,
           pv.export_vaccine_code as vaccine_code,
           pv.vaccine_name as vaccine_name
    from ovst_vaccine ov
    left outer join  person_vaccine pv on ov.person_vaccine_id = pv.person_vaccine_id     
    LEFT OUTER JOIN  ovst o on o.vn = ov.vn
    WHERE o.hn = '${hn}'
        `);
    return data.rows;
  }
  async getProcedure(db: Knex, hn: any, dateServe: any, seq: any) {
    let data = await db.raw(`
    SELECT o.hn as pid,
           o.vn as seq,
           o.vstdate as date_serv,	
           o.vsttime as time_serv,
           od.icd10 as procedure_code,
           ic.name as procedure_name,
           to_char(od.vstdate :: date,'yyyy-mm-dd') as start_date,	
           to_char(od.vsttime :: time,'HH:mm:ss') as start_time,
           to_char(od.vstdate :: date,'yyyy-mm-dd') as end_date,
           '00:00:00' as end_time
    FROM ovst o
    LEFT OUTER JOIN ovstdiag od on od.vn = o.vn
    INNER JOIN icd9cm1 ic on ic.code = od.icd10 
    WHERE o.vn = '${seq}' and o.an is null
    GROUP BY o.vn,od.icd10,o.hn,o.vstdate,o.vsttime,ic.name,od.vstdate,od.vsttime
UNION
    SELECT o.hn as pid,
           o.vn as seq,
           o.vstdate as date_serv,	
           o.vsttime as time_serv,
           i.icd9 as procedure_code,
           c.name as procedure_name,
           to_char(i.opdate :: date,'yyyy-mm-dd') as start_date,	
           to_char(i.optime :: time,'HH:mm:ss') as start_time,
           to_char(i.enddate :: date,'yyyy-mm-dd') as end_date,
           '00:00:00' as end_time
    FROM ovst o
    inner JOIN iptoprt i on i.an = o.an
    INNER JOIN icd101 c on c.code = i.icd9 
    WHERE o.vn = '${seq}'
    GROUP BY o.vn,i.icd9,o.hn,o.vstdate,o.vsttime,c.name,i.opdate,i.optime,i.enddate
        `);
    return data.rows;
  }

  async getNurture(db: Knex, hn: any, dateServe: any, seq: any) {
    let data = await db.raw(`
    select o.vn as seq,
           to_char(o.vstdate :: date,'yyyy-mm-dd') as date_serv, 
           to_char(o.vsttime :: time,'HH:mm:ss') as time_serv, 
           p.bloodgrp as bloodgrp,
           s.bw as weight,
           s.height as height,
           s.bmi as bmi,
           s.temperature as temperature,
           s.pulse as pr,
           s.rr as rr,
           s.bps as sbp,
           s.bpd as dbp,
           s.cc as symptom,
           o.main_dep as depcode,
           k.department as department
    FROM ovst as o 
    LEFT JOIN opdscreen as s on o.vn = s.vn
    INNER JOIN kskdepartment k ON k.depcode = o.main_dep 
    INNER JOIN patient as p	ON p.hn = o.hn
    WHERE o.vn = '${seq}'
        `);
    return data.rows;
  }


  async getPhysical(db: Knex, hn: any, dateServe: any, seq: any) {
    let data = await db.raw(`
    SELECT o.vn as seq ,
    o.pe as pe 
    FROM opdscreen o
    WHERE o.vn = '${seq}'
    `);
    return data.rows;
  }

  async getPillness(db: Knex, hn: any, dateServe: any, seq: any) {
    let data = await db.raw(`
    SELECT o.vn as seq , o.hpi as hpi 
    FROM opdscreen as o WHERE o.vn = '${seq}' 
    `);
    return data.rows;
  }

  async getXray(db: Knex, hn: any, dateServe: any, seq: any) {
    let data = await db.raw(`
    SELECT x.request_date as xray_date ,
    x.xray_items_name as xray_name 
    FROM xray_report x    
    WHERE x.vn = '${seq}'    `);
    return data.rows;
  }


  async getBedsharing(db: Knex) {
    let data = await db.raw(`
    select 
    w.ward as ward_code,
    w.name as ward_name,
    count(i.an) as ward_pt,
    count(DISTINCT(b.bedno)) as ward_bed, 
    w.bedcount as ward_standard
    from bedno b  
    left outer join roomno r on r.roomno = b.roomno  
    left outer join ward w on w.ward = r.ward  
    left outer join iptadm a on a.bedno = b.bedno  
    left outer join ipt i on i.an = a.an and i.dchdate is null
    group by w.ward,w.name,w.bedcount
        `);
    return data.rows;
  }

  async getReferOut(db: Knex, start_date: any, end_date: any) {
    let data = await db.raw(`
    SELECT r.vn as seq,r.hn,
           o2.an as an,
           pt.pname as title_name,
           pt.fname as first_name,
           pt.lname as last_name,
           r.refer_number as referno,
           r.refer_date as ReferDate,
           r.refer_hospcode as to_hcode,
           r.refer_point as location_name,
           (CASE WHEN rt.referout_type_id  = '1' THEN '1' ELSE '2' END) as pttype_id,
					 (CASE WHEN rt.referout_type_id = '1' THEN 'Non Trauma' ELSE 'Trauma' END) as typept_name,
           r.referout_emergency_type_id as strength_id,
           (case r.referout_emergency_type_id
                 when 1 then 'Resucitate'
                 when 2 then 'Emergency'
                 when 3 then 'Urgency'
                 when 4 then 'Semi Urgency'
                 when 5 then 'Non Urgency'
                 else '' end
           ) as strength_name,
           h.name as to_hcode_name,
           rr.name as refer_cause,
           to_char(r.refer_time :: time,'HH:mm:ss') as ReferTime,
           d.licenseno as doctor,
           d.name as doctor_name
    FROM referout r
    INNER JOIN patient pt on pt.hn = r.hn
    left outer join ovst o2 on r.vn = o2.an
    LEFT OUTER JOIN referout_type rt on rt.referout_type_id = r.referout_type_id
    LEFT OUTER JOIN doctor d on d.code = r.doctor
    LEFT OUTER JOIN rfrcs  rr on rr.rfrcs = r.rfrcs
    LEFT OUTER JOIN hospcode h on h.hospcode = r.refer_hospcode
    WHERE r.refer_date between '${start_date}' and '${end_date}'
        `);
    return data.rows;
  }

  async getReferBack(db: Knex, start_date: any, end_date: any) {
    let data = await db.raw(`
    SELECT (CASE WHEN o.an is null THEN r.vn ELSE o.an END)	as seq,
           o.an as an,
           pt.hn,
           pt.pname as title_name,
           pt.fname as first_name,
           pt.lname as last_name,
           r.doc_no as referno,
           to_char(r.reply_date_time :: date,'yyyy-mm-dd') as ReferDate,
           r.dest_hospcode as to_hcode,
           h.name as to_hcode_name,
           '' as refer_cause,
					 to_char(r.reply_date_time :: time,'HH:mm:ss') as ReferTime,
           d.licenseno as doctor,
           d.name as doctor_name
    from refer_reply r
    INNER JOIN ovst o on o.vn = r.vn
    INNER JOIN patient pt on pt.hn = o.hn
    LEFT OUTER JOIN hospcode h on h.hospcode = r.dest_hospcode
    LEFT OUTER JOIN doctor d on d.code = o.doctor
    WHERE to_char(r.reply_date_time :: date,'yyyy-mm-dd') between '${start_date}' and '${end_date}'
        `);
    return data.rows;
  }

  async getAppoint(db: Knex, hn: any, app_date: any) {
    let data = await db.raw(`
    SELECT a.vn as seq,
           a.nextdate as receive_apppoint_date,
           a.nexttime as receive_apppoint_beginttime,
           a.endtime as receive_apppoint_endtime,
           a.app_user as receive_apppoint_doctor,
           c.name as receive_apppoint_chinic,
           a.app_cause as receive_text,
           '' as receive_by
    FROM oapp a
    LEFT OUTER JOIN clinic c on c.clinic = a.clinic
    WHERE a.hn = '${hn}' AND a.vstdate = '${app_date}'
        `);
    return data.rows;
  }

  async getDepartment(db: Knex) {
    let data = await db.raw(`
            select depcode,department from kskdepartment
        `);
    return data.rows;
  }

  async getPtHN(db: Knex, cid: any) {
    let data = await db.raw(`
            select hn from patient where cid = '${cid}'
        `);
    return data.rows;
  }

}

