import Knex = require('knex');
const hospcode = process.env.HIS_CODE;

export class HisHosxpv4Model {

  async getLogin(db: Knex, username: any, password: any) {
    let data = await db.raw(`
    select o.loginname as username , 
           name as fullname, 
           (select hospitalcode from opdconfig limit 1) as hcode				
    from opduser o		 
    where  o.loginname = '${username}' 		
           and passweb = md5('${password}')
        `);
    return data[0];
  }

  async getServices(db: Knex, hn: any, seq: any) {
    let data = await db.raw(`
    SELECT 
          o.vn as seq,
          p.pname as title_name,
          p.fname as first_name,
          p.lname as last_name, 
          DATE_FORMAT(date(o.vsttime),'%Y-%m-%d') as date_serv, 
          DATE_FORMAT(time(o.vstdate),'%h:%i:%s') as time_serv, 
          c.department as department
    FROM ovst as o 
    INNER JOIN kskdepartment as c ON c.depcode = o.main_dep
    INNER JOIN patient as p	ON p.hn = o.hn
    WHERE o.hn ='${hn}' and o.vn = '${seq}' 
    UNION
    SELECT
          an.an as seq,
          p.pname as title_name,
          p.fname as first_name,
          p.lname as last_name,
          DATE_FORMAT(date(i.regtime),'%Y-%m-%d') as date_serv,
          DATE_FORMAT(time(i.regdate),'%h:%i:%s') as time_serv,
          w.ward as department
    FROM an_stat as an
    INNER JOIN ward as w ON w.ward = an.ward
    INNER JOIN patient as p ON p.hn = an.hn
    INNER JOIN ipt as i On i.an = an.an
    WHERE an.hn ='${hn}' and an.an = '${seq}'
    `);
    return data[0];
  }

  async getProfile(db: Knex, hn: any) {
    let data = await db.raw(`
    select p.hn as hn, 
    p.cid as cid, 
    p.pname as title_name,
    p.fname as first_name,
    p.lname as last_name,
    s.name as sex,
    oc.name as job,
    p.moopart,
    p.addrpart,
    p.tmbpart,
    p.amppart,
    p.chwpart,
    p.birthday,
    concat(lpad(timestampdiff(year,p.birthday,now()),3,'0'),'-',
    lpad(mod(timestampdiff(month,p.birthday,now()),12),2,'0'),'-',
    lpad(if(day(p.birthday)>day(now()),dayofmonth(now())-(day(p.birthday)-day(now())),day(now())-day(p.birthday)),2,'0')) as age,
    o.pttype as pttype_id,
    t.name as pttype_name,
    o.pttypeno as pttype_no,
    o.hospmain,
    c.name as hospmain_name,
    o.hospsub,
    h.name as hospsub_name,
    p.firstday as registdate,
    p.last_visit as visitdate,
    p.fathername as father_name,
    p.mathername as mother_name,
    p.informrelation as contact_name,
    p.informtel as contact_mobile,
    oc.name as occupation
FROM patient as p 
INNER JOIN ovst as o on o.hn=p.hn
left join pttype as t on o.pttype=t.pttype
left join hospcode as c on o.hospmain=c.hospcode
left join hospcode as h on o.hospsub=h.hospcode
LEFT JOIN sex s on s.code = p.sex 
LEFT JOIN occupation oc on oc.occupation = p.occupation
WHERE p.hn = '${hn}'	 and p.last_visit = o.vstdate
    `);
    return data[0];
  }

  async getHospital(db: Knex, hn: any) {
    let data = await db.raw(`
      SELECT hospitalcode as provider_code,
             hospitalname as provider_name 
      from opdconfig limit 1
     `);
    return data[0];
  }

  async getAllergyDetail(db: Knex, hn: any) {
    let data = await db.raw(`
    SELECT agent as drug_name, 
           symptom as symptom ,
           begin_date as begin_date,
           date(entry_datetime) as daterecord
    FROM opd_allergy
    WHERE hn ='${hn}'`);
    return data[0];
  }

  async getChronic(db: Knex, hn: any) {
    let data = await db.raw(`
    SELECT c.icd10 as icd_code,
           cm.regdate as start_date, 
           IF(i.tname!='', i.tname, "-") as icd_name
    FROM clinicmember as cm 
    inner join clinic c on cm.clinic = c.clinic
    INNER JOIN icd101 as i on i.code = c.icd10
    WHERE cm.hn ='${hn}'`);
    return data[0];
  }

  async getDiagnosis(db: Knex, hn: any, dateServe: any, seq: any) {
    let data = await db.raw(`
    SELECT o.vn as seq, 
    date_format(o.vstdate,'%Y-%m-%d') as date_serv, 
    time(o.vsttime) as time_serv,
    d.icd10 as icd_code,
    i.name as icd_name,
    d.diagtype as diag_type, 
    dt.provisional_dx_text as DiagNote,           
    '' as diagtype_id          
FROM ovst as o
INNER JOIN ovstdiag d on d.vn = o.vn
INNER JOIN icd101 as i on i.code = d.icd10
left JOIN ovstdiag_text dt on o.vn = dt.vn 
WHERE d.vn ='${seq}'
UNION
SELECT  i.an as seq, 
    date_format(i.dchdate,'%Y-%m-%d') as date_serv, 
    time(i.dchtime) as time_serv,
    id.icd10 as icd_code,
    ic.name as icd_name,
    id.diagtype as diag_type, 
    ic.name as DiagNote,           
    id.diagtype as diagtype_id          
FROM ipt i
INNER JOIN iptdiag id  on id.an = i.an
INNER JOIN icd101 ic  on ic.code = id.icd10
WHERE i.an = '${seq}'
`);
    return data[0];
  }

  async getRefer(db: Knex, hn: any, dateServe: any, seq: any) {
    let data = await db.raw(`
    select if(LENGTH(r.vn) >=  9,r.vn,o2.vn) as seq,
           o2.an as an,
           r.hn as pid, 
           r.hn as hn,
           cast(replace(r.refer_number,"/",'-') as char(20) CHARACTER SET UTF8) as referno,
           r.refer_date as ReferDate, 
           r.refer_hospcode as to_hcode, 
           if(r.referout_emergency_type_id='3',1,2) as pttype_id,
           r.refer_cause as strength_id,
          (case r.refer_cause 
                when 1 then 'Life threatening'
                when 2 then 'Emergency'
                when 3 then 'Urgent'
                when 4 then 'Acute'
                when 5 then 'Non acute'         
                else '' end 
          ) as strength_name,
          r.refer_point as location_name,			
          r.refer_point as station_name,
          (select name from hospcode where hospcode = refer_hospcode) as to_hcode_name,
          (select name from refer_cause where id = refer_cause) as refer_cause,
          refer_time as ReferTime,
          (select licenseno  from doctor where code = r.doctor) as doctor,
          (select name  from doctor where code = r.doctor) as doctor_name   
    from referout as r
    left outer join ovst o on r.vn = o.vn
    left outer join ovst o2 on r.vn = o2.an
    HAVING seq  = '${seq}'	
        `);
    return data[0];
  }

  async getDrugs(db: Knex, hn: any, dateServe: any, seq: any) {
    let data = await db.raw(`
SELECT a.seq,a.date_serv,a.time_serv,a.drug_name,a.qty,a.unit,a.usage_line1,a.usage_line2,a.usage_line3
FROM (
select o.vn as seq,
           DATE_FORMAT(date(i.rxdate),'%Y%m%d') as date_serv,
           DATE_FORMAT(time(i.rxtime),'%h:%i:%s') as time_serv,
           d.name as drug_name,
           sum(i.qty) as qty,
           d.units as unit ,
           (select name1 from drugusage g where g.drugusage = i.drugusage) as usage_line1 ,
           (select name2 from drugusage g where g.drugusage = i.drugusage) as usage_line2,
           (select name3 from drugusage g where g.drugusage = i.drugusage) as usage_line3
    FROM ovst o
    inner join opitemrece as i on (o.vn = i.vn)
    Inner Join drugitems as d ON i.icode = d.icode
    WHERE i.vn = '${seq}'
    union
    select o.vn as seq,
           DATE_FORMAT(date(i.rxdate),'%Y%m%d') as date_serv,
           DATE_FORMAT(time(i.rxtime),'%h:%i:%s') as time_serv,
           d.name as drug_name,
           sum(i.qty) as qty,
           d.units as unit ,
           (select name1 from drugusage g where g.drugusage = i.drugusage) as usage_line1 ,
           (select name2 from drugusage g where g.drugusage = i.drugusage) as usage_line2,
           (select name3 from drugusage g where g.drugusage = i.drugusage) as usage_line3
    FROM ovst o
    inner join opitemrece as i on (o.an = i.an)
    Inner Join drugitems as d ON i.icode = d.icode
    WHERE i.an = '${seq}'
    GROUP BY i.icode,i.rxdate
) a
WHERE a.seq is not NULL
    `);
    return data[0];
  }

  async getLabs(db: Knex, hn: any, dateServe: any, seq: any) {
    let data = await db.raw(`
    select receive_date as 'date_serv',
           receive_time as 'time_serv',
           form_name as 'labgroup',
           lab_items_name_ref as 'lab_name',
           replace(replace(lab_order_result,char(13),''),char(10),'') as lab_result,
           li.lab_items_unit as 'unit',
           lo.lab_items_normal_value_ref as standard_result
    from ovst o
    inner join lab_head lh on o.vn = lh.vn or o.an = lh.vn
    inner join lab_order lo on lh.lab_order_number = lo.lab_order_number
    inner join lab_items li on li.lab_items_code = lo.lab_items_code
    where lh.vn = '${seq}' `);
    return data[0];
  }

  async getAppointment(db: Knex, hn: any, dateServ: any, seq: any) {
    let data = await db.raw(`
    SELECT o.vn as seq, 
           o.vstdate as date_serv, 
           o.nextdate as date, 
           o.nexttime as time, 
           (select name from clinic where clinic = o.clinic ) as department, 
           o.app_cause as detail, 
           time(ovst.vsttime) as time_serv
    FROM oapp as o 
    INNER JOIN ovst on ovst.vn = o.vn
    WHERE o.vn ='${seq}'`);
    return data[0];
  }

  async getVaccine(db: Knex, hn: any) {
    let data = await db.raw(`
    SELECT o.vstdate as date_serv,
           o.vsttime as time_serv,
           pv.export_vaccine_code as vaccine_code,
           pv.vaccine_name as vaccine_name
    from ovst_vaccine ov
    left outer join  person_vaccine pv on ov.person_vaccine_id = pv.person_vaccine_id     
    LEFT OUTER JOIN  ovst o on o.vn = ov.vn
    WHERE o.hn = '${hn}'
        `);
    return data[0];
  }
  async getProcedure(db: Knex, hn: any, dateServe: any, seq: any) {
    let data = await db.raw(`
    SELECT o.hn as pid,
           o.vn as seq,
           o.vstdate as date_serv,	
           o.vsttime as time_serv,
           od.icd10 as procedure_code,
           ic.name as procedure_name,
           DATE_FORMAT(date(od.vstdate),'%Y%m%d') as start_date,	
           DATE_FORMAT(time(od.vsttime),'%h:%i:%s') as start_time,
           DATE_FORMAT(date(od.vstdate),'%Y%m%d') as end_date,
           '00:00:00' as end_time
    FROM ovst o
    LEFT OUTER JOIN ovstdiag od on od.vn = o.vn
    INNER JOIN icd9cm1 ic on ic.code = od.icd10 
    WHERE o.vn = '${seq}' and o.an is null
    GROUP BY o.vn,od.icd10
UNION
    SELECT o.hn as pid,
           o.vn as seq,
           o.vstdate as date_serv,	
           o.vsttime as time_serv,
           i.icd9 as procedure_code,
           c.name as procedure_name,
           DATE_FORMAT(date(i.opdate),'%Y%m%d') as start_date,	
           DATE_FORMAT(time(i.optime),'%h:%i:%s') as start_time,
           DATE_FORMAT(date(i.enddate),'%Y%m%d') as end_date,
           '00:00:00' as end_time
    FROM ovst o
    inner JOIN iptoprt i on i.an = o.an
    INNER JOIN icd101 c on c.code = i.icd9 
    WHERE o.vn = '${seq}'
    GROUP BY o.vn,i.icd9  
        `);
    return data[0];
  }

  async getNurture(db: Knex, hn: any, dateServe: any, seq: any) {
    let data = await db.raw(`
    select o.vn as seq,
           DATE_FORMAT(date(o.vstdate),'%Y-%m-%d') as date_serv, 
           DATE_FORMAT(time(o.vsttime),'%h:%i:%s') as time_serv, 
           p.bloodgrp as bloodgrp,
           s.bw as weight,
           s.height as height,
           s.bmi as bmi,
           s.temperature as temperature,
           s.pulse as pr,
           s.rr as rr,
           s.bps as sbp,
           s.bpd as dbp,
           replace(replace(s.cc,char(13),' '),char(10),' ') as symptom,
           o.main_dep as depcode,
           k.department as department
    FROM ovst as o 
    LEFT JOIN opdscreen as s on o.vn = s.vn
    INNER JOIN kskdepartment k ON k.depcode = o.main_dep 
    INNER JOIN patient as p	ON p.hn = o.hn
    WHERE o.vn = '${seq}'
        `);
    return data[0];
  }


  async getPhysical(db: Knex, hn: any, dateServe: any, seq: any) {
    let data = await db.raw(`
    SELECT o.vn as seq ,
           replace(replace(o.pe,char(13),''),char(10),'') as pe 
    FROM opdscreen o
    WHERE o.vn = '${seq}'
        `);
    return data[0];
  }

  async getPillness(db: Knex, hn: any, dateServe: any, seq: any) {
    let data = await db.raw(`
            SELECT o.vn as seq , replace(replace(o.hpi,char(13),''),char(10),'') as hpi 
            FROM opdscreen as o WHERE o.vn = '${seq}' 
        `);
    return data[0];
  }

  async getXray(db: Knex, hn: any, dateServe: any, seq: any) {
    let data = await db.raw(`
    SELECT x.request_date as xray_date ,i.xray_items_name  as xray_name
    FROM xray_report x
    left outer join xray_items i on i.xray_items_code = x.xray_items_code  
    WHERE x.confirm = 'Y'  and x.confirm_read_film <> 'Y' and x.vn = '${seq}' 
    UNION
    SELECT x.request_date as xray_date ,i.xray_items_name  as xray_name
    FROM xray_report x
    left outer join xray_items i on i.xray_items_code = x.xray_items_code  
    WHERE x.confirm = 'Y'  and x.confirm_read_film <> 'Y' and x.an = '${seq}' 
    GROUP BY x.xn     `);
    return data[0];
  }



  async getBedsharing(db: Knex) {
    let data = await db.raw(`
    select 
    w.ward as ward_code,
    w.name as ward_name,
    count(i.an) as ward_pt,
    count(DISTINCT(b.bedno)) as ward_bed, 
    w.bedcount as ward_standard
    from bedno b  
    left outer join roomno r on r.roomno = b.roomno  
    left outer join ward w on w.ward = r.ward  
    left outer join iptadm a on a.bedno = b.bedno  
    left outer join ipt i on i.an = a.an and i.dchdate is null
    group by w.name 
        `);
    return data[0];
  }

  async getReferOut(db: Knex, start_date: any, end_date: any) {
    let data = await db.raw(`
    SELECT r.vn as seq,r.hn,
           o2.an as an,
           pt.pname as title_name,
           pt.fname as first_name, 
           pt.lname as last_name,
           r.refer_number as referno,
           r.refer_date as ReferDate,
           r.refer_hospcode as to_hcode,
           r.refer_point as location_name,	
           if(rt.referout_type_id = '1','1','2') as pttype_id,
           if(rt.referout_type_id = '1' = '1','Non Trauma','Trauma') as typept_name,
           r.referout_emergency_type_id as strength_id, 
           (case r.referout_emergency_type_id
                 when 1 then 'Resucitate'
                 when 2 then 'Emergency'
                 when 3 then 'Urgency'
                 when 4 then 'Semi Urgency'
                 when 5 then 'Non Urgency'
                 else '' end 
           ) as strength_name,
           h.name as to_hcode_name,
           rr.name as refer_cause,
           time(r.refer_time) as ReferTime, 
           d.licenseno as doctor,
           d.name as doctor_name
    FROM referout r
    INNER JOIN patient pt on pt.hn = r.hn
    left outer join ovst o2 on r.vn = o2.an
    LEFT OUTER JOIN referout_type rt on rt.referout_type_id = r.referout_type_id
    LEFT OUTER JOIN doctor d on d.code = r.doctor
    LEFT OUTER JOIN rfrcs  rr on rr.rfrcs = r.rfrcs
    LEFT OUTER JOIN hospcode h on h.hospcode = r.refer_hospcode
    WHERE r.refer_date between '${start_date}' and '${end_date}'
        `);
    return data[0];
  }

  async getReferBack(db: Knex, start_date: any, end_date: any) {
    let data = await db.raw(`
    SELECT if(o.an is null,r.vn,o.an) as seq,
           o.an as an,
           pt.hn,
           pt.pname as title_name,
           pt.fname as first_name, 
           pt.lname as last_name,
           r.doc_no as referno,
           DATE(r.reply_date_time) as ReferDate,
           r.dest_hospcode as to_hcode,
           h.name as to_hcode_name,
           '' as refer_cause,
           time(r.reply_date_time) as ReferTime,
           d.licenseno as doctor,
           d.name as doctor_name
    from refer_reply r
    INNER JOIN ovst o on o.vn = r.vn
    INNER JOIN patient pt on pt.hn = o.hn
    LEFT OUTER JOIN hospcode h on h.hospcode = r.dest_hospcode
    LEFT OUTER JOIN doctor d on d.code = o.doctor
    WHERE date(r.reply_date_time) between '${start_date}' and '${end_date}'
        `);
    return data[0];
  }

  async getAppoint(db: Knex, hn: any, app_date: any) {
    let data = await db.raw(`
    SELECT a.vn as seq,
           a.nextdate as receive_apppoint_date,
           a.nexttime as receive_apppoint_beginttime,
           a.endtime as receive_apppoint_endtime,
           a.app_user as receive_apppoint_doctor,
           group_concat(distinct c.name) as receive_apppoint_chinic,
           group_concat(distinct a.app_cause) as receive_text,
           '' as receive_by
    FROM oapp a
    LEFT OUTER JOIN clinic c on c.clinic = a.clinic
    WHERE a.hn = '${hn}' AND a.vstdate = '${app_date}'
        `);
    return data[0];
  }

  async getDepartment(db: Knex) {
    let data = await db.raw(`
            select depcode,department from kskdepartment
        `);
    return data[0];
  }

  async getPtHN(db: Knex, cid: any) {
    let data = await db.raw(`
            select hn from patient where cid = '${cid}'
        `);
    return data[0];
  }

}

