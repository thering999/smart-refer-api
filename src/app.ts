'use strict';
import * as path from 'path';
let envPath = path.join(__dirname, '../config');
require('dotenv').config({ path: envPath });
import * as express from 'express';
import * as favicon from 'serve-favicon';
import * as logger from 'morgan';
import * as cookieParser from 'cookie-parser';
import * as bodyParser from 'body-parser';
import index from './routes/index';
import servicesRoute from './routes/services';
import bedsharing from './routes/bedsharing';

import rfoutRoute from './routes/ket/referout';
import rfbackRoute from './routes/ket/referback';
import typept from './routes/ket/typept';
import strength from './routes/ket/strength';
import loads from './routes/ket/loads';
import serviceplan from './routes/ket/serviceplan';
import cause_referout from './routes/ket/cause_referout';
import cause_referback from './routes/ket/cause_referback';
import thaiaddress from './routes/ket/thaiaddress';
import office from './routes/ket/co_office';
import token from './routes/ket/token';
import refer_result from './routes/ket/refer_result';
import station from './routes/ket/station';
import attachment_type from './routes/ket/attachment_type';
import attachment from './routes/ket/attachment';
import version from './routes/ket/version';
import referlog from './routes/ket/refer_log';
import evaluate from './routes/ket/evaluate';

import * as ejs from 'ejs';
import { JwtModel } from './models/jwt';
import Knex = require('knex');
import * as cors from 'cors';

const app: express.Express = express();
const jwt = new JwtModel();

//view engine setup

app.set('views', path.join(__dirname, 'views'));
app.engine('.html', ejs.renderFile);
app.set('view engine', 'html');

//uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname,'public','favicon.ico')));
app.use(logger('dev'));

app.use(bodyParser.raw({ limit: '50mb' }));
app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ extended: false, limit: '50mb' }));


app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(cors());

// Check Token 
let checkAuth = (req, res, next) => {
  let token: string = null;
  // console.log(req.headers.authorization);

  if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
    token = req.headers.authorization.split(' ')[1];
  } else if (req.query && req.query.token) {
    token = req.query.token;
  } else {
    token = req.body.token;
  }

  jwt.verify(token)
    .then((decoded: any) => {
      req.token = token;
      req.decoded = decoded;
      next();
    }, err => {
      return res.send({
        ok: false,
        error: 'No token provided.',
        code: 403
      });
    });
}

let dbConnection: Knex.MySqlConnectionConfig = {
  host: process.env.DB_HOST,
  port: +process.env.DB_PORT,
  user: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_NAME
}


app.use((req, res, next) => {
  let connectKnexconfig: any;
  if (process.env.DB_CLIENT === 'mssql') {
    connectKnexconfig = {
      client: process.env.DB_CLIENT,
      connection: dbConnection,
      requestTimeout: 300000
    };
    req.db = Knex(connectKnexconfig);
  } else if (process.env.DB_CLIENT === 'pg') {
    connectKnexconfig = {
      client: process.env.DB_CLIENT,
      searchPath: ['knex', 'public'],
      connection: dbConnection,
    };
    req.db = Knex(connectKnexconfig);
  } else {
    req.db = Knex({
      client: process.env.DB_CLIENT,
      connection: dbConnection,
      pool: {
        min: 0,
        max: 7,
        afterCreate: (conn, done) => {
          conn.query('SET NAMES ' + process.env.DB_CHARSET, (err) => {
            done(err, conn);
          });
        }
      },
      debug: false,
      acquireConnectionTimeout: 1500000
    });
  }

  next();
})

app.use(cors());
app.use('/', index);
app.use('/services', servicesRoute);

app.use('/referout', rfoutRoute);
app.use('/referback', rfbackRoute);
app.use('/typept', typept);
app.use('/strength', strength);
app.use('/cause_referback', cause_referback);
app.use('/cause_referout', cause_referout);
app.use('/loads', loads);
app.use('/address', thaiaddress);
app.use('/office', office);
app.use('/bedsharing', bedsharing);
app.use('/token', token);
app.use('/serviceplan', serviceplan);
app.use('/result', refer_result);
app.use('/station', station);
app.use('/attachment_type', attachment_type);
app.use('/attachment', attachment);
app.use('/version', version);
app.use('/referlog', referlog);
app.use('/evaluate', evaluate);

//catch 404 and forward to error handler
app.use((req, res, next) => {
  var err = new Error('Not Found');
  err['status'] = 404;
  next(err);
});

app.use((err, req, res, next) => {
  console.log(err);
  let errorMessage;
  switch (err['code']) {
    case 'ER_DUP_ENTRY':
      errorMessage = 'ข้อมูลซ้ำ';
      break;
    default:
      errorMessage = err;
      res.status(err['status'] || 500);
  }
  res.send({ ok: false, error: errorMessage });
});

export default app;
