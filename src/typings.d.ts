import express = require('express');
import Knex = require('knex');

declare global {
  namespace Express {
    export interface Request {
      token: any;
      db: Knex;
      dbRf: Knex;
      decoded: any
    }
  }
}